# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

## Eclipse ダウンロードのヒント

Eclipseのダウンロードページでは、古いバージョンなどもあり初めてダウンロードするときは迷うこともあります。

どれをダウンロードしたら良いかを迷ったときは、下記を参考にダウンロードしましょう。

- Eclipseのバージョン選択
 - ![Eclipseバージョン選択](images/eclipse_download_01.png)


- Eclipseのパッケージ選択
 - ![Eclipseパッケージ選択](images/eclipse_download_02.png)
