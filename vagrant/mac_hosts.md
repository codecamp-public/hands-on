# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# (Mac版) 3. localhost以外のアクセス

## 3-2. 概要

Web開発では一般的に、本番環境・ステージング環境・テスト環境・開発環境などのように、環境を分けることが多いです。

以下は、仮に本番環境のドメインが `example.com` だとした場合の構築例です。

|環境|FQDN|作られる場所|用途|
|---|---|---|---|
|本番|example.com|インターネット上|本番公開用|
|ステージング|stg.example.com|インターネット上|本番とほぼ同じ構成を保ちソースをテストする環境|
|テスト|test.example.com|インターネット上 or 社内ネットワーク上|構成変更などを含めテストする環境|
|開発|dev.example.com|ローカルPC|各実装者の開発環境|

## 3-3. hostsファイルの修正

前述のような構成において、`dev.example.com` でVagrant環境にアクセスできるようにするためには `hosts` ファイルを修正することで実現できます。  
＊下記コマンド実行時、パスワードを聞かれたらMacのパスワードを入力してください。  
＊下記コマンドは２回以上実行しないこと。

```bash
$ echo -e "\n127.0.0.1\tdev.example.com"|sudo tee -a /etc/hosts
```

設定されたことの確認は下記コマンドを実行します。

```bash
$ cat /etc/hosts
```

下記のように出力されて入ればOKです。

```
127.0.0.1       localhost
255.255.255.255	broadcasthost
::1             localhost

127.0.0.1       dev.example.com
```

実行後、下記にアクセスして正常に表示されることを確認しましょう。  
[http://dev.example.com](http://dev.example.com)
