# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# (Mac版) 2. ローカル環境の構築

## 2-1. 作成するディレクトリ構成

今回構築するディレクトリ構成は下記の通りです。

```
workspace
  └── hands-on
      └── src
          ├── Vagrantfile
          └── www
              └── html
                  └── phpinfo.php

```

## 2-2. Vagrant環境の作成

### 2-2-1. インストール

インストールは下記の順番で実施します。(特に順番に意味はありません)  
インストーラーに従ってインストールしてください。

- VirtualBox
- Vagrant

＊ High SierraにてVirtualBoxをインストールできないという報告があります。  
VirtualBoxのインストールが途中で止まってしまう場合は、このページ最後にあるトラブルシューティングを確認してください。

### 2-2-2. インストールの確認方法

- VirtualBox
  1. `Finder -> アプリケーション` に `VirtualBox` があることを確認（実行しなくて良いです）
- Vagrant
  1. `Finder -> アプリケーション -> ユーティリティ` から `ターミナル` を実行
  1. ターミナルで `vagrant -v` を実行。バージョンが表示される

### 2-2-3. 仮想PCの導入

#### 2-2-3-1. 仮想PC導入の流れ

下記を順に実行していきましょう。

1. workspace ディレクトリの作成
1. サービスディレクトリを作成
1. vagrant初期化ファイルの作成
1. vagrant初回起動
1. webサーバーへのアクセス設定

#### 2-2-3-2. workspace ディレクトリの作成

workspaceとは、開発を実施するためのベースとなるディレクトリです。  
複数サービスなどを開発する場合に、workspaceを決めておくことで管理が容易になります。

ターミナルで下記コマンドを実行

```bash
$ mkdir ~/Documents/workspace
```

#### 2-2-3-3. サービスディレクトリを作成

サービスディレクトリはその名の通り、１つのWebサービスを作成するためのプロジェクトディレクトリ名となります。  

ターミナルで下記コマンドを実行

```bash
$ mkdir -p ~/Documents/workspace/hands-on/src
```

#### 2-2-3-4. vagrant初期化ファイルの作成

vagrantの起動に必要な `Vagrantfile` を作成します。

ターミナルで下記コマンドを実行

```bash
$ cd ~/Documents/workspace/hands-on/src
$ vagrant init dayspring-tech/dayspring-centos6-php7-js-mysql57 --box-version 1.0.0
```

`vagrant init` 実行後はこんな出力がされます。

```
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

`vagrant init` に成功するとVagrantfileができています。  
下記コマンドで内容ファイルが作成されていることを確認しましょう。

```bash
$ cat ./Vagrantfile
```

以降の作業でこのファイルを編集することがあるため、念のためバックアップしておきます。

```bash
$ cp -p ./Vagrantfile ./Vagrantfile.bakup
```

#### 2-2-3-5. vagrant初回起動

ターミナルで下記コマンドを実行

```bash
$ vagrant up
```

<div style="background-color: lightgray!important;"><pre>
<img src="images/common/coffee-640.png" width="60px" height="60px" style="border: none;">ちょっと一息
初回の `vagrant up` には少し時間がかかります。  
ここで少し休憩を入れましょう。 (５分〜１０分ほど)</pre></div>

`vagrant up` 実行後はこんな出力がされます。

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: 1.0.0
==> default: Loading metadata for box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'
    default: URL: https://vagrantcloud.com/dayspring-tech/dayspring-centos6-php7-js-mysql57
==> default: Adding box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/dayspring-tech/boxes/dayspring-centos6-php7-js-mysql57/versions/1.0.0/providers/virtualbox.box
    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
==> default: Successfully added box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for 'virtualbox'!
==> default: Importing base box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' is up to date...
==> default: Setting the name of the VM: src_default_1543045849656_74290
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 5.0.20
    default: VirtualBox Version: 5.2
==> default: Mounting shared folders...
    default: /vagrant => /Users/demo001/Documents/workspace/hands-on/src
```

#### 2-2-3-6. Vagrant の起動・終了

Vagrantの起動・終了は下記の通りです。  
なお、Vagrantfileと同じディレクトリで実行する必要があります。

Vagrantの起動

```
$ vagrant up
```

Vagrantの終了

```
$ vagrant halt
```

Vagrantの再起動  
＊ halt -> up の順で実行するのと同じ

```
$ vagrant reload
```

Vagrantの起動状況確認

```
$ vagrant status
```



#### 2-2-3-7. webサーバーへのアクセス設定

初期設定のままだと、ブラウザから仮想PCへのアクセスができません。
下記を実行して、ブラウザから仮想PCのWebサーバーにアクセスできるようにしましょう。

まずはVagrantのプラグインを導入します。

```bash
$ vagrant plugin install vagrant-triggers
```

下記のように出力されます。

```bash
Installing the 'vagrant-triggers' plugin. This can take a few minutes...
Fetching: bundler-1.17.1.gem (100%)
Fetching: vagrant-triggers-0.5.3.gem (100%)
Installed the plugin 'vagrant-triggers (0.5.3)'!
```

続いて、Vagrantfileの設定を変更します。  
`vi` の使い方に慣れていない人もいると思うので、 `テキストエディタ` で編集をしましょう。  
下記コマンドで、VagrantfileのあるディレクトリをFinderで開きましょう。

```bash
$ open ./
```

Finderが表示されたら、`Vagrantfile -> このアプリケーションで開く -> テキストエディタ` で開き、ファイルの一番最後の `end` の１行上に下記を追記しましょう。

```bash
  config.vm.network "forwarded_port", guest: 80, host: 8080

  config.trigger.after [:provision, :up, :reload] do
    system('echo "rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 80 -> 127.0.0.1 port 8080" | sudo pfctl -ef - > /dev/null 2>&1')
    system('echo "set packet filter 127.0.0.1:80 -> 127.0.0.1:8080"')
  end

  config.trigger.after [:halt, :destroy] do
    system("sudo pfctl -df /etc/pf.conf > /dev/null 2>&1")
    system('echo "reset packet filter"')
  end
```

追記したら、下記コマンドでVagrantを再起動しましょう。  
＊途中、パスワードを聞かれたらMacのパスワードを入力してください。

```bash
$ vagrant reload
```

<div style="background-color: lightgray!important;"><pre>
<b>【Tips: Macのポートフォワード】</b>
Mac環境のVagrantでは1024以下のポートフォワードが設定できないため、triggersプラグインを利用してポートフォワードを実現しています。
Windows環境では下記１行のみの記述で設定できます。
　
config.vm.network "forwarded_port", guest: 80, host: 80</pre></div>


## トラブルシューティング

### VirtualBoxがインストールできない

High SierraではVirtualboxがインストールができないことがあるようです。

「拡張機能がブロックされました」というエラーが出てインストールが中止された場合は下記手順で解決しててください。

1. `システム環境設定 -> セキュリティーとプライバシー` を開いて `一般のタブ` を表示する
1. `ダウンロードしたアプリケーションの実行許可：`の項目を確認
1. `開発元”Oracle America, Inc.”のシステム・ソフトウェアの読み込みがブロックされました。` と表示されていた場合 `許可` をクリック
 - ![(mac)VirtualBoxがインストールできない](images/mac/virtualbox_install_trouble.png)


上記で解決できない場合は別の問題の可能性があります。
