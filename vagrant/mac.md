# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# Vagrantでローカル環境構築 [Mac版]

## アジェンダ

- ダウンロード
- 目的
- 付録
- １. Vagrantの仕組
- ２. ローカル環境の構築
- ３. localhost以外のアクセス
- ４. ホストOSとゲストOSのファイル連携
- ５. Eclipse PDT の導入

## ダウンロード

解説に入る前に、ダウンロードをしましょう。

- [VirtualBox](https://www.virtualbox.org/)
  - ダウンロードサイズ: 約33mb
  - インストールサイズ: 約96mb
- [Vagrant](https://www.vagrantup.com/)
  - ダウンロードサイズ: 約96mb
  - インストールサイズ: 約183mb
- [Eclipse](http://mergedoc.osdn.jp/)
  - Mac 64bit Full Edition PHPを選択
  - ダウンロードサイズ: 約541mb
  - インストールサイズ: 約821mb

## 目的

Web開発をする場合、手っ取り早くローカル環境を構築する方法としてMAMPやXAMPPなどがあります。

ただし、これらの方法では開発環境と実施に動作する環境が異なることによる問題が発生することがあるため、より本番に近い環境で実装するのが理想的です。

本番に近い環境とは

- OSが同じであること
- Webサーバーなどのミドルウェアが同じであること
- ミドルウェアのバージョンが同じであること
- ミドルウェアの設定が同じであること

などが挙げられます。

今日は、一般的な開発環境をお手元のパソコンに手っ取り早く導入する方法として、Vagrantの導入を一緒に実践してみましょう。

## 1. Vagrantの仕組

下記資料を参照。  
[1. Vagrantの仕組](vagrant.md)

## 2. ローカル環境の構築

### 2-1. 作成するディレクトリ構成

今回構築するディレクトリ構成は下記の通りです。

```
workspace
  └── hands-on
      └── src
          ├── Vagrantfile
          └── www
              └── html
                  └── phpinfo.php

```

### 2-2. Vagrant環境の作成

#### 2-2-1. インストール

インストールは下記の順番で実施します。(特に順番に意味はありません)  
インストーラーに従ってインストールしてください。

- Vagrant
- VirtualBox

#### 2-2-2. インストールの確認方法

- Vagrant
  1. `Finder -> アプリケーション -> ユーティリティ` から `ターミナル` を実行
  1. ターミナルで `vagrant -v` を実行。バージョンが表示される
- VirtualBox
  1. `Finder -> アプリケーション` に `VirtualBox` があることを確認（実行しなくて良いです）

#### 2-2-3. 仮想PCの導入

##### 2-2-3-1. 仮想PC導入の流れ

下記を順に実行していきましょう。

1. workspace ディレクトリの作成
1. サービスディレクトリを作成
1. vagrant初期化ファイルの作成
1. vagrant初回起動
1. webサーバーへのアクセス設定

##### 2-2-3-2. workspace ディレクトリの作成

workspaceとは、開発を実施するためのベースとなるディレクトリです。  
複数サービスなどを開発する場合に、workspaceを決めておくことで管理が容易になります。

ターミナルで下記コマンドを実行

```bash
$ mkdir ~/Documents/workspace
```

##### 2-2-3-3. サービスディレクトリを作成

サービスディレクトリはその名の通り、１つのWebサービスを作成するためのプロジェクトディレクトリ名となります。  

ターミナルで下記コマンドを実行

```bash
$ mkdir -p ~/Documents/workspace/hands-on/src
```

##### 2-2-3-4. vagrant初期化ファイルの作成

vagrantの起動に必要な `Vagrantfile` を作成します。

ターミナルで下記コマンドを実行

```bash
$ cd ~/Documents/workspace/hands-on/src
$ vagrant init dayspring-tech/dayspring-centos6-php7-js-mysql57 --box-version 1.0.0
```

`vagrant init` 実行後はこんな出力がされます。

```
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

`vagrant init` に成功するとVagrantfileができています。  
下記コマンドで内容ファイルが作成されていることを確認しましょう。

```bash
$ cat ./Vagrantfile
```

以降の作業でこのファイルを編集することがあるため、念のためバックアップしておきます。

```bash
$ cp -p ./Vagrantfile ./Vagrantfile.bakup
```

##### 2-2-3-5. vagrant初回起動

ターミナルで下記コマンドを実行

```bash
$ vagrant up
```

<div style="background-color: lightgray!important;"><pre>
<img src="images/common/coffee-640.png" width="60px" height="60px" style="border: none;">ちょっと一息
初回の `vagrant up` には少し時間がかかります。  
ここで少し休憩を入れましょう。 (５分〜１０分ほど)</pre></div>

`vagrant up` 実行後はこんな出力がされます。

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: 1.0.0
==> default: Loading metadata for box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'
    default: URL: https://vagrantcloud.com/dayspring-tech/dayspring-centos6-php7-js-mysql57
==> default: Adding box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/dayspring-tech/boxes/dayspring-centos6-php7-js-mysql57/versions/1.0.0/providers/virtualbox.box
    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
==> default: Successfully added box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for 'virtualbox'!
==> default: Importing base box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' is up to date...
==> default: Setting the name of the VM: src_default_1543045849656_74290
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 5.0.20
    default: VirtualBox Version: 5.2
==> default: Mounting shared folders...
    default: /vagrant => /Users/demo001/Documents/workspace/hands-on/src
```

##### 2-2-3-6. Vagrant の起動・終了

Vagrantの起動・終了は下記の通りです。  
なお、Vagrantfileと同じディレクトリで実行する必要があります。

Vagrantの起動

```
$ vagrant up
```

Vagrantの終了

```
$ vagrant halt
```

Vagrantの再起動  
＊ halt -> up の順で実行するのと同じ

```
$ vagrant reload
```

Vagrantの起動状況確認

```
$ vagrant status
```



##### 2-2-3-7. webサーバーへのアクセス設定

初期設定のままだと、ブラウザから仮想PCへのアクセスができません。
下記を実行して、ブラウザから仮想PCのWebサーバーにアクセスできるようにしましょう。

まずはVagrantのプラグインを導入します。

```bash
$ vagrant plugin install vagrant-triggers
```

下記のように出力されます。

```bash
Installing the 'vagrant-triggers' plugin. This can take a few minutes...
Fetching: bundler-1.17.1.gem (100%)
Fetching: vagrant-triggers-0.5.3.gem (100%)
Installed the plugin 'vagrant-triggers (0.5.3)'!
```

続いて、Vagrantfileの設定を変更します。  
`vi` の使い方に慣れていない人もいると思うので、 `テキストエディタ` で編集をしましょう。  
下記コマンドで、VagrantfileのあるディレクトリをFinderで開きましょう。

```bash
$ open ./
```

Finderが表示されたら、`Vagrantfile -> このアプリケーションで開く -> テキストエディタ` で開き、ファイルの一番最後の `end` の１行上に下記を追記しましょう。

```bash
  config.vm.network "forwarded_port", guest: 80, host: 8080

  config.trigger.after [:provision, :up, :reload] do
    system('echo "rdr pass on lo0 inet proto tcp from any to 127.0.0.1 port 80 -> 127.0.0.1 port 8080" | sudo pfctl -ef - > /dev/null 2>&1')
    system('echo "set packet filter 127.0.0.1:80 -> 127.0.0.1:8080"')
  end

  config.trigger.after [:halt, :destroy] do
    system("sudo pfctl -df /etc/pf.conf > /dev/null 2>&1")
    system('echo "reset packet filter"')
  end
```

追記したら、下記コマンドでVagrantを再起動しましょう。  
＊途中、パスワードを聞かれたらMacのパスワードを入力してください。

```bash
$ vagrant reload
```

<div style="background-color: lightgray!important;"><pre>
<b>【Tips: Macのポートフォワード】</b>
Mac環境のVagrantでは1024以下のポートフォワードが設定できないため、triggersプラグインを利用してポートフォワードを実現しています。
Windows環境では下記１行のみの記述で設定できます。
　
config.vm.network "forwarded_port", guest: 80, host: 80</pre></div>

## 3. localhost以外のアクセス

### 3-2. 概要

Web開発では一般的に、本番環境・ステージング環境・テスト環境・開発環境などのように、環境を分けることが多いです。

以下は、仮に本番環境のドメインが `example.com` だとした場合の構築例です。

|環境|FQDN|作られる場所|用途|
|---|---|---|---|
|本番|example.com|インターネット上|本番公開用|
|ステージング|stg.example.com|インターネット上|本番とほぼ同じ構成を保ちソースをテストする環境|
|テスト|test.example.com|インターネット上 or 社内ネットワーク上|構成変更などを含めテストする環境|
|開発|dev.example.com|ローカルPC|各実装者の開発環境|

### 3-3. hostsファイルの修正

前述のような構成において、`dev.example.com` でVagrant環境にアクセスできるようにするためには `hosts` ファイルを修正することで実現できます。  
＊下記コマンド実行時、パスワードを聞かれたらMacのパスワードを入力してください。  
＊下記コマンドは２回以上実行しないこと。

```bash
$ echo -e "\n127.0.0.1\tdev.example.com"|sudo tee -a /etc/hosts
```

設定されたことの確認は下記コマンドを実行します。

```bash
$ cat /etc/hosts
```

下記のように出力されて入ればOKです。

```
127.0.0.1       localhost
255.255.255.255	broadcasthost
::1             localhost

127.0.0.1       dev.example.com
```

実行後、下記にアクセスして正常に表示されることを確認しましょう。  
[http://dev.example.com](http://dev.example.com)


## 4. ホストOSとゲストOSのファイル連携

### 4-1. 概要

Vagrantでは、デフォルトの設定では、Vagrantfileが保存されているディレクトリ以下が、ゲストOSでは `/vagrant` というディレクトリにマッピングされていて
ホストOSとゲストOSがファイルを連携できるように設定されています。

そのため、 `/vagrant` ディレクトリの配下に Webサーバーの `DocumentRoot` を設定することで、ホストOSで実装したファイルをブラウザからすぐに確認することができるようになります。

<div style="background-color: lightgray!important;"><pre>
<b>【Tips: DocumentRoot】</b>
DocumentRootとは公開するディレクトリを指定するためのWebサーバーの設定のことです。
今回Webサーバーとして採用されているApacheでは、デフォルトが "/var/www/html" となっています。</pre></div>

### 4-2. DocumentRootの変更

この作業はゲストOS上で実施します。  
下記コマンドを実行して、ゲストOSにログインします。

```bash
$ vagrant ssh
```

下記のような表示がされればログインが成功しています。

```bash
[vagrant@vagrant-centos65 ~]$
```

新しいDocumentRootとなるディレクトリを作成します。

```bash
$ mkdir -p /vagrant/www/html
```

作成したディレクトリのフルパスをDocumentRootに指定します。

```bash
$ sudo sed -i -e 's/DocumentRoot "\/var\/www\/html"/DocumentRoot "\/vagrant\/www\/html"/g' /etc/httpd/conf/httpd.conf
```

変更後は設定を読み込ませる必要があるため、Webサーバーを再起動します。

```bash
$ sudo /etc/init.d/httpd restart
```

## 5. Eclipse PDT の導入

＊ 以降、Eclipeで統一

### 5-1. Eclipse のインストール

ダウンロードしたEclipseのzipを解凍しましょう。  
解凍後は、Eclipseをアプリケーションにコピーします。

### 5-2. Eclipseの初回実行

初回起動時には `workspace` の設定を求められます。  
先ほど作成したworkspaceをフルパスで指定して、 `この選択をデフォルトとして使用し、...` にチェックを入れて `起動` ボタンを押します。

### 5-3. プロジェクトの作成

下記手順でプロジェクトを作成します。

1. 画面左側の `プロジェクト・エクスプローラー` を右クリック
1. `新規 -> プロジェクト` を選択
1. `ウィザードを選択` 画面で `PHP プロジェクト` を選択して `次へ >` を押す
1. `PHP プロジェクトの作成`画面で下記の通り入力
   - プロジェクト名: hands-on
   - PHP バージョン: 7.2を選択
   - その他の項目: デフォルトのまま

### 5-4. phpのファイルを追加

下記手順でphpファイルを作成します。

1. 画面左側の `プロジェクト・エクスプローラー` で、 `hands-on -> src -> www -> html` とディレクトリを展開
1. `html` ディレクトリを右クリック
1. `新規 -> PHP ファイル` を選択
1. ファイル名に `phpinfo.php` を指定
1. 下記２行を記述して保存

```php
<?php
phpinfo();
```

作業が完了したら、下記にアクセスしましょう。  
[http://dev.example.com/phpinfo.php](http://dev.example.com/phpinfo.php)

実行中のPHPの設定一覧が表示されます。

## お疲れ様でした

以上で、Vagrantでローカル環境構築ハンズオンは終了です。

## 付録

### このハンズオンで登場するコマンド

このハンズオンで登場するOSコマンドです。  
オプションの説明は、このハンズオンで登場したオプションです。

|コマンド|Mac|Linux|説明|
|---|---|---|---|
|mkdir|○|○|ディレクトリを作成。<br> -p オプションをつけることで、指定した階層を全て作成。|
|cd|○|○|カレントディレクトリの移動。|
|cat|○|○|ファイルの中身を確認。|
|vi|○|○|ファイルを編集するコマンド。<br>今回のハンズオンでは利用していない。|
|open|○|×|Finderを表示するためのコマンド。<br>Linuxでは利用できない。|
|echo|○|○|指定した文字列を出力。<br> -e オプションをつけることで改行コードなどが利用できるようになる。|
|tee|○|○|受け取った文字列を出力しつつ、ファイルに保存する。<br> -a オプションをつけることで追記モードで保存する。|
|sudo|○|○|管理者権限モードで実行する|
|/etc/init.d/httpd|×|○|Apacheの起動を管理するためのシェル。<br> start/stop/restart/etc... などのオプションと組み合わせて利用する。|
|sed|○|○|文字列の置換をする。<br> -i オプションで上書き保存、 -e オプションで 置換パターンを指定する|
