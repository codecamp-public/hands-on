# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# (Win版) 2. ローカル環境の構築

## 2-1. 作成するディレクトリ構成

今回構築するディレクトリ構成は下記の通りです。

```
workspace
  └── hands-on
      └── src
          ├── Vagrantfile
          └── www
              └── html
                  └── phpinfo.php

```

## 2-2. Vagrant環境の作成

### 2-2-1. インストール

インストールは下記の順番で実施します。(特に順番に意味はありません)  
インストーラーに従ってインストールしてください。

- Vagrant
- VirtualBox

### 2-2-2. インストールの確認方法

- Vagrant
  1. `Windowsキー + R` で `ファイル名を指定して実行` を開く
  1. `powershell` を入力して `Enter`
  1. PowerShellウィンドウで `vagrant -v` を実行。バージョンが表示される。
- VirtualBox
  1. `Windowsキー` から `virtualbox` と入力
  1. `もっとも一致する検索結果` に ` Oracle VM VirtualBox` があればOK。（起動しなくて良いです）

### 2-2-3. 仮想PCの導入

#### 2-2-3-1. 仮想PC導入の流れ

下記を順に実行していきましょう。

1. workspace ディレクトリの作成
1. サービスディレクトリを作成
1. vagrant初期化ファイルの作成
1. vagrant初回起動
1. webサーバーへのアクセス設定

#### 2-2-3-2. workspace ディレクトリの作成

<div style="background-color: lightgray!important;"><pre>
<b>【注意: ドライブの指定】</b>
このハンズオンでは便宜上<b>Cドライブ</b>にworkspaceを作成します。
しかし、システムドライブにworkspaceを作成するのはあまりオススメしません。
Cドライブ以外のデータドライブがあるなら、workspaceはそちらに作成しましょう。</pre></div>


workspaceとは、開発を実施するためのベースとなるディレクトリです。  
複数サービスなどを開発する場合に、workspaceを決めておくことで管理が容易になります。

ターミナルで下記コマンドを実行

```powershell
$ mkdir c:¥workspace
```

#### 2-2-3-3. サービスディレクトリを作成

サービスディレクトリはその名の通り、１つのWebサービスを作成するためのプロジェクトディレクトリ名となります。  

ターミナルで下記コマンドを実行

```powershell
$ mkdir c:\workspace\hands-on\src
```

#### 2-2-3-4. vagrant初期化ファイルの作成

vagrantの起動に必要な `Vagrantfile` を作成します。

ターミナルで下記コマンドを実行

```powershell
$ cd ~\Documents\workspace\hands-on\src
$ vagrant init dayspring-tech/dayspring-centos6-php7-js-mysql57 --box-version 1.0.0
```

`vagrant init` 実行後はこんな出力がされます。

```
A `Vagrantfile` has been placed in this directory. You are now
ready to `vagrant up` your first virtual environment! Please read
the comments in the Vagrantfile as well as documentation on
`vagrantup.com` for more information on using Vagrant.
```

`vagrant init` に成功するとVagrantfileができています。  
下記コマンドで内容ファイルが作成されていることを確認しましょう。

```powershell
$ cat .\Vagrantfile
```

以降の作業でこのファイルを編集することがあるため、念のためバックアップしておきます。

```powershell
$ cp -p .\Vagrantfile .\Vagrantfile.bakup
```

#### 2-2-3-5. vagrant初回起動

ターミナルで下記コマンドを実行

```powershell
$ vagrant up
```

<div style="background-color: lightgray!important;"><pre>
<img src="images/common/coffee-640.png" width="60px" height="60px" style="border: none;">ちょっと一息
初回の `vagrant up` には少し時間がかかります。  
ここで少し休憩を入れましょう。 (５分〜１０分ほど)</pre></div>

`vagrant up` 実行後はこんな出力がされます。

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: 1.0.0
==> default: Loading metadata for box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'
    default: URL: https://vagrantcloud.com/dayspring-tech/dayspring-centos6-php7-js-mysql57
==> default: Adding box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/dayspring-tech/boxes/dayspring-centos6-php7-js-mysql57/versions/1.0.0/providers/virtualbox.box
    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
==> default: Successfully added box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for 'virtualbox'!
==> default: Importing base box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' is up to date...
==> default: Setting the name of the VM: src_default_1543045849656_74290
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 5.0.20
    default: VirtualBox Version: 5.2
==> default: Mounting shared folders...
    default: /vagrant => /Users/demo001/Documents/workspace/hands-on/src
```

#### 2-2-3-6. Vagrant の起動・終了

Vagrantの起動・終了は下記の通りです。  
なお、Vagrantfileと同じディレクトリで実行する必要があります。

Vagrantの起動

```
$ vagrant up
```

Vagrantの終了

```
$ vagrant halt
```

Vagrantの再起動  
＊ halt -> up の順で実行するのと同じ

```
$ vagrant reload
```

Vagrantの起動状況確認

```
$ vagrant status
```

#### 2-2-3-7. webサーバーへのアクセス設定

初期設定のままだと、ブラウザから仮想PCへのアクセスができません。
下記を実行して、ブラウザから仮想PCのWebサーバーにアクセスできるようにしましょう。

下記コマンドで、Vagrantfileを `テキストエディタ` で編集をして設定を変更します。  

```powershell
$ notepad .\Vagrantfile
```

テキストエディタが表示されたら、ファイルの一番最後の `end` の１行上に下記を追記しましょう。

```powershell
  config.vm.network "forwarded_port", guest: 80, host: 80
```

追記したら、下記コマンドでVagrantを再起動しましょう。  

```powershell
$ vagrant reload
```

<div style="background-color: lightgray!important;"><pre>
<b>【Tips: Macのポートフォワード】</b>
Mac環境のVagrantでは1024以下のポートフォワードが設定できないため、少しややこしい設定が必要です。

詳しくは下記を参照してください。  
[2-2-3-7. webサーバーへのアクセス設定
](https://gitlab.com/codecamp-public/hands-on/blob/master/vagrant/mac_vagrant.md#2-2-3-7-web%E3%82%B5%E3%83%BC%E3%83%90%E3%83%BC%E3%81%B8%E3%81%AE%E3%82%A2%E3%82%AF%E3%82%BB%E3%82%B9%E8%A8%AD%E5%AE%9A)</pre></div>
