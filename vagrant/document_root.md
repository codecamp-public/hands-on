# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# 4. ホストOSとゲストOSのファイル連携

## 4-1. 概要

Vagrantでは、デフォルトの設定では、Vagrantfileが保存されているディレクトリ以下が、ゲストOSでは `/vagrant` というディレクトリにマッピングされていて
ホストOSとゲストOSがファイルを連携できるように設定されています。

そのため、 `/vagrant` ディレクトリの配下に Webサーバーの `DocumentRoot` を設定することで、ホストOSで実装したファイルをブラウザからすぐに確認することができるようになります。

<div style="background-color: lightgray!important;"><pre>
<b>【Tips: DocumentRoot】</b>
DocumentRootとは公開するディレクトリを指定するためのWebサーバーの設定のことです。
今回Webサーバーとして採用されているApacheでは、デフォルトが "/var/www/html" となっています。</pre></div>

## 4-2. DocumentRootの変更

この作業はゲストOS上で実施します。  
下記コマンドを実行して、ゲストOSにログインします。
＊パスワードを聞かれたら `vagrant` と入力します。

```
$ vagrant ssh
```

下記のような表示がされればログインが成功しています。

```bash
[vagrant@vagrant-centos65 ~]$
```

新しいDocumentRootとなるディレクトリを作成します。

```bash
$ mkdir -p /vagrant/www/html
```

作成したディレクトリのフルパスをDocumentRootに指定します。

```bash
$ sudo sed -i -e 's/DocumentRoot "\/var\/www\/html"/DocumentRoot "\/vagrant\/www\/html"/g' /etc/httpd/conf/httpd.conf
```

変更後は設定を読み込ませる必要があるため、Webサーバーを再起動します。

```bash
$ sudo /etc/init.d/httpd restart
```
