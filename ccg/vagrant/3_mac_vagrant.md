# 3. ローカル環境の構築

## 3-1. 作成するディレクトリ構成

今回構築するディレクトリ構成は下記の通りです。

```
workspace
└── ccg
    ├── Vagrantfile
    └── syncs
        └── vagrant
            ├── lib
            │   └── php
            │       ├── opcache
            │       ├── session
            │       └── wsdlcache
            └── www
                ├── dev.lesson-codecamp.jp
                │   └── webroot
                │       └── index.html
                └── tool.lesson-codecamp.jp
                    └── webroot
                        ├── index.html
                        ├── phpinfo.php
                        └── pma
                            ├── ＊phpMyAdminのファイル群
                            ├── ...
                            └── ...
```

## 3-2. Vagrant環境の作成

### 3-2-1. インストール

インストールは下記の順番で実施します。(特に順番に意味はありません)  
インストーラーに従ってインストールしてください。

- Vagrant
- VirtualBox

### 3-2-2. インストールの確認方法

- Vagrant
  1. `Finder -> アプリケーション -> ユーティリティ` から `ターミナル` を実行
  1. ターミナルで `vagrant -v` を実行。バージョンが表示される
- VirtualBox
  1. `Finder -> アプリケーション` に `VirtualBox` があることを確認（実行しなくて良いです）

### 3-2-3. 仮想PCの導入

#### 3-2-3-1. 仮想PC導入の流れ

下記は、仮想PC導入の流れです。

1. workspace ディレクトリの作成
1. 必要ファイルのダウンロード
1. vagrant初期化ファイルの作成
1. vagrant初回起動
1. webサーバーへのアクセス設定

#### 3-2-3-1. workspace ディレクトリの作成

workspaceとは、開発を実施するためのベースとなるディレクトリです。  
複数サービスなどを開発する場合に、workspaceを決めておくことで管理が容易になります。

ターミナルで下記コマンドを実行

```bash
$ mkdir -p ~/Documents/workspace/ccg
$ cd ~/Documents/workspace/ccg
```

#### 3-2-3-2. 必要ファイルのダウンロード ＆ 展開

必要ファイルとはccgようにカスタマイズされたVagrantの設定ファイルなどを圧縮したファイルのことです。

ファイルをダウンロード ＆ 展開するために、ターミナルで下記コマンドを順に実行してください。

```bash
$ curl -o ./ccg.zip https://s3-ap-northeast-1.amazonaws.com/codecamp-gate/vagrant/ccg.zip
$ unzip ./ccg.zip
```

#### 3-2-3-4. vagrant初回起動

vagrantは `Vagrantfile` というファイルで諸々の設定をします。 　
本来はこの設定ファイルも自分で作成するのですが、今回はccg学習用にすでに設定されています。

初回起動時では、設定ファイルに基づきベースとなる仮装PCのイメージをダウンロードしてゲストOSを起動します。

それでは下記コマンドをターミナルで実行し、初回起動をしましょう。

```bash
$ vagrant up
```

<div style="background-color: lightgray!important;"><pre>
<img src="images/common/coffee-640.png" width="60px" height="60px" style="border: none;">ちょっと一息
初回の `vagrant up` には少し時間がかかります。  
ここで少し休憩を入れましょう。 (５分〜１０分ほど)</pre></div>

`vagrant up` 実行後はこんな出力がされます。

```
Bringing machine 'default' up with 'virtualbox' provider...
==> default: Box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' could not be found. Attempting to find and install...
    default: Box Provider: virtualbox
    default: Box Version: 1.0.0
==> default: Loading metadata for box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'
    default: URL: https://vagrantcloud.com/dayspring-tech/dayspring-centos6-php7-js-mysql57
==> default: Adding box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for provider: virtualbox
    default: Downloading: https://vagrantcloud.com/dayspring-tech/boxes/dayspring-centos6-php7-js-mysql57/versions/1.0.0/providers/virtualbox.box
    default: Download redirected to host: vagrantcloud-files-production.s3.amazonaws.com
==> default: Successfully added box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' (v1.0.0) for 'virtualbox'!
==> default: Importing base box 'dayspring-tech/dayspring-centos6-php7-js-mysql57'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'dayspring-tech/dayspring-centos6-php7-js-mysql57' is up to date...
==> default: Setting the name of the VM: src_default_1543045849656_74290
Vagrant is currently configured to create VirtualBox synced folders with
the `SharedFoldersEnableSymlinksCreate` option enabled. If the Vagrant
guest is not trusted, you may want to disable this option. For more
information on this option, please refer to the VirtualBox manual:

  https://www.virtualbox.org/manual/ch04.html#sharedfolders

This option can be disabled globally with an environment variable:

  VAGRANT_DISABLE_VBOXSYMLINKCREATE=1

or on a per folder basis within the Vagrantfile:

  config.vm.synced_folder '/host/path', '/guest/path', SharedFoldersEnableSymlinksCreate: false
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
    default: Adapter 1: nat
==> default: Forwarding ports...
    default: 22 (guest) => 2222 (host) (adapter 1)
==> default: Booting VM...
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 127.0.0.1:2222
    default: SSH username: vagrant
    default: SSH auth method: private key
    default:
    default: Vagrant insecure key detected. Vagrant will automatically replace
    default: this with a newly generated keypair for better security.
    default:
    default: Inserting generated public key within guest...
    default: Removing insecure key from the guest if it's present...
    default: Key inserted! Disconnecting and reconnecting using new SSH key...
==> default: Machine booted and ready!
==> default: Checking for guest additions in VM...
    default: The guest additions on this VM do not match the installed version of
    default: VirtualBox! In most cases this is fine, but in rare cases it can
    default: prevent things such as shared folders from working properly. If you see
    default: shared folder errors, please make sure the guest additions within the
    default: virtual machine match the version of VirtualBox you have installed on
    default: your host and reload your VM.
    default:
    default: Guest Additions Version: 5.0.20
    default: VirtualBox Version: 5.2
==> default: Mounting shared folders...
    default: /vagrant => /Users/demo001/Documents/workspace/hands-on/src
```

### 3-2-4. Vagrantの操作

Vagrantの基本的な操作は下記の通りです。  
なお、Vagrantfileと同じディレクトリで実行する必要があります。

ここで紹介しているコマンド以外にも便利な機能もあります。  
詳しくは公式サイトを確認しましょう。  
[Vagrant公式ドキュメント:コマンド（英語）](https://www.vagrantup.com/docs/cli/)

Vagrantの起動

```
$ vagrant up
```

Vagrantの終了

```
$ vagrant halt
```

Vagrantの再起動  
＊ halt -> up の順で実行するのと同じ

```
$ vagrant reload
```

Vagrantの起動状況確認

```
$ vagrant status
```

起動したVagrantにログイン

```
$ vagrant ssh
```
