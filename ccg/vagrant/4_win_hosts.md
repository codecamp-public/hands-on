# 4. localhost以外のアクセス

## 4-2. 概要

Web開発では一般的に、本番環境・ステージング環境・テスト環境・開発環境などのように、環境を分けることが多いです。

以下は、仮に本番環境のドメインが `lesson-codecamp.jp` だとした場合の構築例です。

|環境|FQDN|作られる場所|用途|
|---|---|---|---|
|本番|lesson-codecamp.jp|インターネット上|本番公開用|
|ステージング|stg.lesson-codecamp.jp|インターネット上|本番とほぼ同じ構成を保ちソースをテストする環境|
|テスト|test.lesson-codecamp.jp|インターネット上 or 社内ネットワーク上|構成変更などを含めテストする環境|
|開発|dev.lesson-codecamp.jp|ローカルPC|各実装者の開発環境|

## 4-3. hostsファイルの修正

前述のような構成において、`dev.lesson-codecamp.jp` でVagrant環境にアクセスできるようにするためには `hosts` ファイルを修正することで実現できます。  

Windowsでhostsファイルを修正するには管理者権限が必要です。  
まずは、PowerShellを管理者権限で開きます。

```powershell
$ start powershell -Verb runas
```

新しく開いたPowerShellでhostsファイルをテキストエディタで開きます。

```powershell
$ notepad ${env:SystemRoot}\sytem32\drivers\etc\hosts
```

テキストエディタが開いたら下記一文を追加して上書き保存してください。

```
127.0.0.1       dev.lesson-codecamp.jp
127.0.0.1       stg.lesson-codecamp.jp
127.0.0.1       tool.lesson-codecamp.jp
```

実行後、下記にアクセスして正常に表示されることを確認しましょう。  
[http://dev.lesson-codecamp.jp:8080](http://dev.lesson-codecamp.jp:8080)
