# 2. 構築するvagrantの情報

今回構築されるVagrantの詳細です。

## 2-1. OS/ミドルウェアのバージョン

|項目|バージョン|
|---|---|
|OS|CentOS 7系|
|Webサーバー|Apache 2.4|
|PHP|PHP 7.3|
|DB|MySQL 5.7|

## 2-2. ポートフォワード

ポートフォワードとは、ホストOSに届いたポートをゲストOSに送ることを言います。

今回のVagrantでは下記の通り設定されています。  
＊デフォルトの設定は記載していません

|ホストOS|ゲストOS|説明|
|---|---|---|
|8080|80|http通信|
|8443|443|https通信|

## 2-3. ディレクトリの同期

VirtualBoxの機能を利用して、ホストOSで更新したファイルをゲストOSでリアルタイムに同期させることができます。

|ホストOS|ゲストOS|
|---|---|
|$workspace/ccg/syncs|/vagrant|

## 2-4. バーチャルホスト

バーチャルホストとは、複数のホストを一つのWebサーバーで扱うための設定です。

|ホスト名|用途|
|---|---|
|dev.lesson-codecamp.jp|開発用|
|stg.lesson-codecamp.jp|デプロイを含めた動作確認用|
|tool.lesson-codecamp.jp|phpMyAdminなどの利用|

以下ホスト名を、dev / stg / tool と省略

|ホスト名|ドキュメントルート|ホストOSとの同期|
|---|---|---|
|dev|/vagrant/www/dev.lesson-codecamp.jp/webroot|あり|
|stg|/var/www/stg.lesson-codecamp.jp/webroot|なし|
|tool|/vagrant/www/tool.lesson-codecamp.jp/webroot|あり|

|ホスト名|http URL|https URL|
|---|---|---|
|dev|[http://dev.lesson-codecamp.jp:8080](http://dev.lesson-codecamp.jp:8080)|[https://dev.lesson-codecamp.jp:8443](https://dev.lesson-codecamp.jp:8443)|
|stg|[http://stg.lesson-codecamp.jp:8080](http://stg.lesson-codecamp.jp:8080)|[https://stg.lesson-codecamp.jp:8443](https://stg.lesson-codecamp.jp:8443)|
|tool|[http://tool.lesson-codecamp.jp:8080](http://tool.lesson-codecamp.jp:8080)|[https://tool.lesson-codecamp.jp:8443](https://tool.lesson-codecamp.jp:8443)|

## 2-5. tool.lesson-codecamp.jp

tool.lesson-codecamp.jpには下記が含まれています。

- [phpMyAdmin](http://tool.lesson-codecamp.jp:8080/pma)
- [phpinfo](http://tool.lesson-codecamp.jp:8080/phpinfo.php)

## 2-6. データベース

以下は、データベースの設定です。

|ユーザ|パス|説明|
|---|---|---|
|root|@CodeCamp0|管理者権限|
|codecamp_user|@CodeCamp0|Webシステムで利用|

|データベース名|説明|
|---|---|
|codecamp_db|Webシステムで利用|
