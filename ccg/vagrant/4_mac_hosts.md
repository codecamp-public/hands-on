# 4. localhost以外のアクセス

## 4-2. 概要

Web開発では一般的に、本番環境・ステージング環境・テスト環境・開発環境などのように、環境を分けることが多いです。

以下は、仮に本番環境のドメインが `lesson-codecamp.jp` だとした場合の構築例です。

|環境|FQDN|作られる場所|用途|
|---|---|---|---|
|本番|lesson-codecamp.jp|インターネット上|本番公開用|
|ステージング|stg.lesson-codecamp.jp|インターネット上|本番とほぼ同じ構成を保ちソースをテストする環境|
|テスト|test.lesson-codecamp.jp|インターネット上 or 社内ネットワーク上|構成変更などを含めテストする環境|
|開発|dev.lesson-codecamp.jp|ローカルPC|各実装者の開発環境|

## 4-3. hostsファイルの修正

前述のような構成において、`dev.lesson-codecamp.jp` でVagrant環境にアクセスできるようにするためには `hosts` ファイルを修正することで実現できます。  
＊下記コマンド実行時、パスワードを聞かれたらMacのパスワードを入力してください。  
＊下記コマンドは２回以上実行しないこと。

```bash
$ echo -e "\n127.0.0.1\tdev.lesson-codecamp.jp\n127.0.0.1\tstg.lesson-codecamp.jp\n127.0.0.1\ttool.lesson-codecamp.jp"|sudo tee -a /etc/hosts
```

設定されたことの確認は下記コマンドを実行します。

```bash
$ cat /etc/hosts
```

下記のように出力されて入ればOKです。

```
127.0.0.1       localhost
255.255.255.255	broadcasthost
::1             localhost

127.0.0.1       dev.lesson-codecamp.jp
127.0.0.1       stg.lesson-codecamp.jp
127.0.0.1       tool.lesson-codecamp.jp
```

実行後、下記にアクセスして正常に表示されることを確認しましょう。  
[http://dev.lesson-codecamp.jp:8080](http://dev.lesson-codecamp.jp:8080)
