# マークダウンでサクッとメモれるようになる勉強会

## マークダウン崩れしないポイント

1. インライン要素の前後には半角スペースを入れる
    - 行頭・行末には不要
1. ブロック要素の前後には空の改行を入れる
1. **改行にはスペース2個** を忘れない
1. プレビュー機能を利用する
1. アップロード後にチェックする
   - 特に改行とブロック要素崩れは見落とされがち

インライン要素、ブロック要素とは

1. インライン要素
    - イタリックなどの「その他色々」で紹介した文字装飾
    - リンク、画像
1. ブロック要素
    - タイトル
    - テーブル
    - リスト
    - ソース
    - 引用
    - 水平線

### lintの導入

lintとは記述ミスをチェックするためのツールの総称です。  
VSCではマークダウン用のlintを簡単に導入することができます。

下記を参考にlintを導入します。  
再起動は不要です。

![lintのインストール](./images/install_lint.png)

デフォルトの設定で好みに合わない時は、設定ファイルを拡張します。

以下、参考までに森山の設定です。

```json
{
    "html": false,
    "MD013": false,
    "MD007": {
        "indent": 4
    }
}
```

この内容を、プロジェクト直下に `.markdownlint.json` という名前で保存します。

設定している内容は下記の通りです。

|項目|値|説明|
|---|---|---|
|html|false|`<br>`などを使いたいため、htmlのチェックをしない|
|MD013|false|１行80文字のチェックをしない|
|MD007|"indent": 4|リストのインデントは空白4つ|

---

## CodeCampのマークダウン

CodeCampで仕様しているマークダウンは通常のマークダウンと一部仕様が異なっています。

教材を書く機会がある場合は下記を一読するようにしましょう。

[CodeCampカリキュラムのMarkdownルール](https://gitlab.com/codecamp-teacher/textbook_md/blob/develop/README.md)

---

## マークダウンの便利な使い方

### 簡単に成形されたPDFを作成

slackとChrome拡張を組み合わせて利用することで、簡単に装飾されたPDF資料を作成することができます。

[Chrome拡張: Markdown Preview Plus](https://chrome.google.com/webstore/detail/markdown-preview-plus/febilkbfcbhebfnokafefeacimjdckgl?hl=ja)

Chrome拡張さえ事前にインストールされていれば、3ステップで簡単に作成できます。

1. slackでスニペットを作成
    - タイトルに拡張子 `.md` を入れる
1. RAWデータを表示
1. Chromeの印刷機能でPDF化

[簡単PDF](./images/convert_pdf.gif)  
![簡単PDF](./images/convert_pdf.gif)

### 体系化されたページの生成

マークダウンで記述することで、成形したドキュメントを容易に公開することができます。

下記ページはGitBookと言うコンバーターを利用することで、体系化された資料を生成しています。

![成形したドキュメント](./images/ccg_text.png)

[GitBook](https://www.gitbook.com/)
