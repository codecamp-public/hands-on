# hands-on

## ライセンス

このコンテンツは、ディレクトリごとのREADMEに明記がない限り **Creative Commons BY-SA 4.0** にて公開されています。  
[Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)

## ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

