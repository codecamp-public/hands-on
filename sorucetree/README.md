# ライセンスのための表記

```
このコンテンツはコードキャンプ株式会社により作成され、Creative Commons BY-SA 4.0により公開されています。
https://codecamp.jp/
```

# Sourcetree

## ダウンロード

下記からダウンロード  
[Sourcetreeダウンロード](https://ja.atlassian.com/software/sourcetree)

## インストール

### Mac

1. ダウンロードしたzipファイルを解凍
1. 解凍された `Sourcetree` を実行
1. 次のような画像が出たら `Move to Application Folder` をクリック
   - ![move_to_application_folder](images/move_to_application_folder.png)
1. インストーラーが起動するので、 `Bitbucketクラウド` をクリック
   - ![インストーラー/登録](images/installer_01.png)
1. `ATLASSIAN` のサインイン画面が表示されるので、サインイン or サインアップをする
   - まだアカウントがない場合、Googleアカウントで作成するのが良い
   - ![ATLASSIAN/サインイン](images/atlassinan_login.png)
1. サインイン完了後、Sourcetreeを開くかと聞かれるので `Sourcetreeを開く` を押す
   - ![ATLASSIAN/サインイン後](images/atlassian_after_login.png)
1. Sourcetreeに戻ると登録が完了した旨のメッセージが表示されるので '続行' を押す
   - ![インストーラー/登録完了](images/installer_02.png)
1. `Preferences` 画面ではそのまま　`完了` ボタンを押す
   - ![インストーラー/設定](images/installer_03.png)
1. Sourcetreeが起動したら左上のメニューから `URLからクローン` を選択
   - **＊注意：後述するNo.11の認証も正しいのにURLを認識しない時はトラブルシューティングを参照**
      - [トラブルシューティング：URLを認識しない](#urlを認識しない)
   - ![URLからクローン](images/clone_from_url.png)
1. `リポジトリをクローン` ダイアログが表示されたら、まずは `ソースURL` を入力。
   - ![リポジトリからクローン](images/clone_dialog.png)
1. `ソースURL` を入力するとログインダイアログが表示されるので、クローン元のログイン情報を入力
   - **＊注意：このログイン情報を間違えると、入力し直しが面倒なので注意**
   - ＊ログイン情報を誤ってしまった場合は、トラブルシューティングを参照
      - [トラブルシューティング：クローン時にログイン情報を間違えた](#クローン時にログイン情報を間違えた)
   - ＊ここで入力されたユーザ情報は保存され、同一サービス上でのクローンに再利用されます。
   - ![gitログインID](images/gitlab_loginid.png)
   - ![gitログイン](images/git_login.png)
1. クローン元へのログインが成功すると、下記のように `これはGitリポジトリです` と表示される
   - ![gitログイン後](images/git_after_login.png)
1. `保存先のパス` と `名前` を入力して `クローン` を実行。クローンが完了すると、リポジトリ画面が表示される
   - 名前はSourcetree上のリポジトリ一覧に表示されます。（あとで変更可能）
   - ![リポジトリ](images/repository.png)

## トラブルシューティング

### クローン時にログイン情報を間違えた

クローン時にログイン情報を間違えると、間違えた情報を記憶してしまうために、同一gitサーバーからのクローンができなくなってしまいます。  
＊ 何度クローンしようとしても `ソースパス、または...正しくありません` と表示されてしまう。 　
![ログインミス](images/missing_login.png)

ログイン情報を間違えてしまった場合は、下記手順で誤ったログイン情報を削除して実行し直します。

1. `Sourcetree` メニューから `環境設定` を選択
   - ![Sourcetree環境設定メニュー](images/sourcetree_environment_01.png)
1. `環境設定画面` 右上の `高度な設定` をクリック
   - ![高度な設定](images/sourcetree_environment_02.png)
1. 一覧の対象となるホスト名を選択して `削除` をクリック。確認ダイアログが出るので `OK` をクリック
   - ![削除確認](images/confirm_delete_user.png)
   - 下記は削除後の画面
   - ![ユーザーを削除後](images/sourcetree_environment_03.png)
1. ユーザを削除したら今一度、クローンをし直す。
   - ![URLからクローン](images/clone_from_url.png)

### URLを認識しない

認証情報はあっているのに、`ソースパス、または...正しくありません` と表示されてしまう時は、URLが間違っている可能性があります。

指定するURLは下記の例の通り、 `.git` で終わるものを指定します。

- 例）gitのURL  
  - https://gitlab.com/codecamp-public/hands-on.git

GitLabでは下記の通り操作することでHTTPSのURLをコピーすることができます。

![URLのコピー](images/copy_url.png)
